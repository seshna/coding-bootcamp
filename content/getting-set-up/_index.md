---
title: "Getting Set Up"
date: 2022-01-22T20:12:57+08:00
weight: 1
chapter: true
pre: "<i class='fas fa-code fa-fw'></i> "
---

### Chapter 0

# Getting Set Up

This chapter will set up your coding environment. You will: 

#### 1. Download, install, and set up an app called **Visual Studio Code**.
#### 2. Create some directories for where all of your coding projects will go. 

