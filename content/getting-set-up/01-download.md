---
title: "An app for coding"
weight: 1
draft: false
---

#### 1. Download [Visual Studio Code](https://code.visualstudio.com/Download). This will be your IDE[^IDE]

![stormtroopocat](https://upload.wikimedia.org/wikipedia/commons/thumb/9/9a/Visual_Studio_Code_1.35_icon.svg/2048px-Visual_Studio_Code_1.35_icon.svg.png?width=15pc&classes=shadow&featherlight=false)


[^IDE]: IDE - Integrated Developer Environment or the app you use to write code with

#### 2. Find where it downloaded to, and double click to install it

