---
title: "Create Directories"
weight: 2
draft: false
---

1. Create all these new sub-directories  


```
    Documents
    └── src                     
        └── coding-bootcamp    
```



[^IDE]: IDE - Integrated Developer Environment or the app you use to write code with