---
title: "Visual Studio Code Settings"
draft: false
weight: 1
---

## Turn off Hover

<ol class="todo">
  <li>
    <label>
      <input type="checkbox">Open Settings using shortcut keys <kbd>⌘</kbd> + <kbd>,</kbd>
    </label>
  </li>
  <li>
    <label>
      <input type="checkbox">Uncheck <strong>Editor > Hover: Enabled</strong></li>
    </label>
  </li>
  {{< todo "testing 123" >}}
</ol>
