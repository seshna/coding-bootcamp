---
title: "Project: Mestats"
draft: false
weight: 7
---

## Me Stats
Log fitness related statistics, like measurements 

1. Create a new file, call it `mestats.html`  
    ```bash
    Documents
    └── src                     
        └── coding-bootcamp
            └── index.html
            └── mestats.html <- new file
    ```

2. Open the file and insert the template html code tags
  `doctype, html, head, body`
  {{%expand "Click for the answer" %}}
  ```html
  <!DOCTYPE html>
  <html>
    <head>
    </head>

    <body>
    </body>
  </html> 
  ```
  {{% /expand%}}

3. In the `<head>` section, create `<title>` tags enclosing "Mestats".  
  {{% expand "Click for the answer" %}}
  ```html
  <!DOCTYPE html>
  <html>
    <head>
      <title>Mestats</title>
    </head>

    <body>
    </body>
  </html> 
  ```    
  {{% /expand%}}

4. Create a heading in the `<body>` section, call it "Measurements"
  {{% expand "Click for the answer" %}}
  ```html
  <!DOCTYPE html>
  <html>
    <head>
      <title>Mestats</title>
    </head>

    <body>
      <h1>Measurements</h1>
    </body>
  </html> 
  ```
  {{% /expand%}}

5. Create a table that looks similar to below, but with your own columns titles, then add your own measurements in
    ![Simple table](https://i.imgur.com/0J0lvKi.png?width=30pc&classes=shadow&featherlight=true)
    You may notice 2 new things:
    1. The table has a `<caption>` "2022 Dates and measurements"
    2. The **Date** cell is spanning 2 columns  
        `<th colspan=2>Date</th>`
    2. The **Feb** cell is spanning 4 rows  
        `<th rowspan=4>Feb</th>`

{{% expand "Click for the answer" %}}
```html
  <!DOCTYPE html>
  <html>
    <head>
      <title>Mestats</title>
    </head>

    <body>
      <div>
        <h1>Mestats</h1>
        <h2>Measurements</h2>

        <table border=1 cellpadding=10>
          <caption>2022 Dates and measurements</caption>
          <thead>
            <tr>
              <th colspan=2>Date</th>
              <th>Weight (kg)</th>
              <th>Arms (cm)</th>
              <th>Chest (cm)</th>
              <th>Waist (cm)</th>
              <th>Hips (cm)</th>
            </tr>
          </thead>
          <tbody>
            <tr>
              <th rowspan=4>Feb</th>
              <th>Mon 7th</th>
              <td></td>
              <td></td>
              <td></td>
              <td></td>
              <td></td>
            </tr>
            <tr>
              <th>Mon 14th</th>
              <td></td>
              <td></td>
              <td></td>
              <td></td>
              <td></td>
            </tr>
            <tr>
              <th>Mon 21st</th>
              <td></td>
              <td></td>
              <td></td>
              <td></td>
              <td></td>
            </tr>
            <tr>
              <th>Mon 28th</th>
              <td></td>
              <td></td>
              <td></td>
              <td></td>
              <td></td>
            </tr>
            <tr>
              <th rowspan=4>Mar</th>
              <th>Mon 4th</th>
              <td></td>
              <td></td>
              <td></td>
              <td></td>
              <td></td>
            </tr>
          </tbody>
        </table>
      </div>
    </body>
  </html>
```
{{% /expand%}}


That concludes basic html. Next you'll learn how to style your html page with cascading style sheets.
