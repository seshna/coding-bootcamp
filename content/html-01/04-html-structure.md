---
title: "HTML Structure"
draft: false
weight: 5
---

## Anatomy

Just like we saw with a table, an entire HTML page also has a structure

```html
<!DOCTYPE html>
<html>
  <head>
  </head>

  <body>
  </body>
</html> 
```

### &lt;!DOCTYPE html>

A web browser can read all sorts of files. You can't just trust that it will inherently know what it's reading just because your file ends in `.html`. So, we explicitly declare it using `<!DOCTYPE html>`.
You'll notice this does not have a closing tag.

### &lt;html>

All of your content will go in here, generally within the `<body>` section.

### &lt;head>

This contains important information about your **html** document, which we will look into next for **CSS** (**C**ascading **S**tyle **S**heets)


### &lt;body>

This is where you put all of the content people can see in the document.

So your final task is to put all of your html code within the `<body>`.


{{%expand "Click to show the code" %}}
```html
<!DOCTYPE html>
<html>
  <head>
  </head>

  <body>
    <h1>Shaun Cechner</h1>
    <p>
        <em>Sometimes</em> people put <code>code</code>
        on separate lines to make things <b>easier to read</b>.
    </p>
    <p><small>only if they want to.</small></p>

    <table border=1 cellpadding=10px>
      <thead bgcolor=red>
        <tr>
          <th>Name</th>
          <th>Birthday</th>
          <th>Cat or Dog Person</th>
        </tr>
      </thead>

      <tbody>
        <tr>
          <td>Shaun</td>
          <td>12th July</td>
          <td>Dog</td>
        </tr>
        <tr bgcolor=grey>
          <td>Kelly</td>
          <td>30th March</td>
          <td>Cat</td>
        </tr>
        <tr>
          <td>Matt</td>
          <td>12th July</td>
          <td>Dog</td>
        </tr>
      </tbody>
    </table>
  </body>
</html> 
```
{{% /expand%}}
