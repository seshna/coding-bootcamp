---
title: "Visual Studio Code Settings"
draft: false
weight: 1
---

## Turn off HTML Auto Closing Tags

When you first use Visual Studio Code, will notice when you write a `<tag>`, the editor will automatically create a closing `</tag>` for you. 

![Annoying auto closing tags](https://i.imgur.com/pvlr3wF.gif)

If, like me, you don't like this behaviour, the video below shows you how to turn it off.

![Turn off html auto closing tags](https://i.imgur.com/cq2eGfh.gif)

{{%expand "Click for written instructions" %}}
<ol class="todo">
  {{< todo "Click the bottom left gear icon" >}}
  {{< todo "Click 'Settings'" >}}
  {{< todo "Search Settings - 'html auto'" >}}
  {{< todo "Uncheck the 'HTML: Auto Closing Tags' option" >}}
  {{< todo "Close the Settings tab 'X'" >}}
</ol>
{{% /expand%}}