---
title: "Tables and Attributes"
draft: false
weight: 4
---

## Tables

We're going to construct this simple table

![Simple table](https://i.imgur.com/jgdivoS.png?width=10pc&classes=shadow&featherlight=true)


First construct the table tags

```html
<table>

</table>
```

A table can be made of 3 parts
- Table head `<thead>`
- Table body `<tbody>`
- Table foot `<tfoot>` (I've never used this, so I'll leave it out)

```html
<table>
  <thead>
  </thead>

  <tbody>
  </tbody>
</table>
```

{{< highlight html "linenos=table,hl_lines=2-6,linenostart=1" >}}
<table>
  <thead>
  </thead>

  <tbody>
  </tbody>
</table>
{{< / highlight >}}

First we'll make the table header.  
Every new row needs to go between `<tr>` `</tr>` tags
Every cell in the row needs to go either between: 
- `<th>` `</th>` tags (for **bold text**) or
- `<td>` `</td>` tags (for normal text)

```html
<table>
  <thead>
    <tr>
      <th>Name</th>
      <th>Birthday</th>
    </tr>
  </thead>

  <tbody>
  </tbody>
</table>
```

Here's what it should look like

![Simple table](https://i.imgur.com/CXHLOX5.png?classes=shadow&featherlight=true)

You most likely noticed there is no border - we will add that later.  
For now, lets put in the rest of the rows in the `<tbody>` section

```html
<table>
  <thead>
    <tr>
      <th>Name</th>
      <th>Birthday</th>
    </tr>
  </thead>

  <tbody>
    <tr>
      <td>Shaun</td>
      <td>12th July</td>
    </tr>
    <tr>
      <td>Kelly</td>
      <td>30th March</td>
    </tr>
  </tbody>
</table>
```

![Simple table](https://i.imgur.com/XZVDMSq.png?classes=shadow&featherlight=true)

Now try:
1. Put in an extra **column**. So you have the columns **Name**, **Birthday** and **Cat or Dog Person**
1. Put in an extra row or two

{{%expand "Click for answer" %}}
```html
<table>
  <thead>
    <tr>
      <th>Name</th>
      <th>Birthday</th>
      <th>Cat or Dog Person</th>
    </tr>
  </thead>

  <tbody>
    <tr>
      <td>Shaun</td>
      <td>12th July</td>
      <td>Dog</td>
    </tr>
    <tr>
      <td>Kelly</td>
      <td>30th March</td>
      <td>Cat</td>
    </tr>
    <tr>
      <td>Matt</td>
      <td>12th July</td>
      <td>Dog</td>
    </tr>
  </tbody>
</table>
```
{{% /expand%}}

---


## Attributes

Each tag has:
- a name  
eg. `<table>`'s name is **table**
- 0 or more attributes
- an attribute has a *key* and a *value*

A tag with attributes looks like this:

```html
<table border=1>
```

This tag above has 1 attribute, **border**.
- **Border** is the **key** of the attribute
- **1** is the **value** of the attribute

You can guess what it does to our table


![Simple table](https://i.imgur.com/wDDMYAJ.png?classes=shadow&featherlight=true)


{{%expand "Click to show the code" %}}
```html
<table border=1>
  <thead>
    <tr>
      <th>Name</th>
      <th>Birthday</th>
      <th>Cat or Dog Person</th>
    </tr>
  </thead>

  <tbody>
    <tr>
      <td>Shaun</td>
      <td>12th July</td>
      <td>Dog</td>
    </tr>
    <tr>
      <td>Kelly</td>
      <td>30th March</td>
      <td>Cat</td>
    </tr>
    <tr>
      <td>Matt</td>
      <td>12th July</td>
      <td>Dog</td>
    </tr>
  </tbody>
</table>
```
{{% /expand%}}

Some other examples of attributes you can use with `<table>` are:
- `cellpadding=10px` (gives each cell padding of 10 pixels)
- `bgcolor=red` (changes the background colour of the whole table to red)

![Simple table](https://i.imgur.com/Ils6ONm.png?classes=shadow&featherlight=true)


{{%expand "Click to show the code" %}}
```html
<table border=1 cellpadding=10px bgcolor=red>
  <thead>
    <tr>
      <th>Name</th>
      <th>Birthday</th>
      <th>Cat or Dog Person</th>
    </tr>
  </thead>

  <tbody>
    <tr>
      <td>Shaun</td>
      <td>12th July</td>
      <td>Dog</td>
    </tr>
    <tr>
      <td>Kelly</td>
      <td>30th March</td>
      <td>Cat</td>
    </tr>
    <tr>
      <td>Matt</td>
      <td>12th July</td>
      <td>Dog</td>
    </tr>
  </tbody>
</table>
```
{{% /expand%}}

Now try
1. Make only the header row red
2. Make every 2nd body row grey

![Simple table](https://i.imgur.com/3wBPjYn.png?classes=shadow&featherlight=true)

{{%expand "Click to show the code" %}}
```html
<table border=1 cellpadding=10px>
  <thead bgcolor=red>
    <tr>
      <th>Name</th>
      <th>Birthday</th>
      <th>Cat or Dog Person</th>
    </tr>
  </thead>

  <tbody>
    <tr>
      <td>Shaun</td>
      <td>12th July</td>
      <td>Dog</td>
    </tr>
    <tr bgcolor=grey>
      <td>Kelly</td>
      <td>30th March</td>
      <td>Cat</td>
    </tr>
    <tr>
      <td>Matt</td>
      <td>12th July</td>
      <td>Dog</td>
    </tr>
  </tbody>
</table>
```
{{% /expand%}}

