---
title: "HTML Level 1"
weight: 2
chapter: true
pre: "<i class='fab fa-html5 fa-fw'></i> "
---

### Chapter 1

# Writing your first .html file

In this short chapter you will...
- Set up Visual Studio Code to be slightly less annoying
- Create a new html file
- Do basic text formatting
- Create Tables
- Learn Html attributes
  - Adding borders and colours to tables
- Add html structure
- Create a small project to test yourself