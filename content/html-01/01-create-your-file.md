---
title: "Create your first HTML file"
draft: false
weight: 2
---

1. Open Visual Studio Code
2. Open your project folder
   1. Click **File > Open Folder...**  
![Open Folder...](https://i.imgur.com/pVUTxZq.png?width=30pc&classes=shadow&featherlight=true)
   2. Navigate to `Documents > src > coding-bootcamp`  
    ```
        Documents
        └── src                     
            └── coding-bootcamp    <- here
    ```
   3. Click **Open** 
3. Create a new file  
  ![New File](https://i.imgur.com/LTqb1EU.gif)  
  {{%expand "Click for written instructions" %}}
  - Click **File > New File**  
  - Click **File > Save**  or <key>COMMAND + S</key>  
  - Type `index.html`  
  - Click **Save**
  {{% /expand%}}
