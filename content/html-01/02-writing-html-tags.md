---
title: "Getting to know tags"
draft: false
weight: 3
---

## Code and Browser windows

When writing HTML, it is handy to open a web browser to the side of your IDE, so you can constantly refresh it to see what your changes look like.

![Side by side](https://i.imgur.com/eHxt9iI.png?width=30pc&classes=shadow&featherlight=true)

## Writing text

When reading HTML files, browsers can understand basic text and display it for you to see.

1. Write your name in the IDE, and save it
2. Refresh the browser
3. Do you see your name? (you should)

![Your name in a browser](https://i.imgur.com/5QcKiG6.png?width=30pc&classes=shadow&featherlight=true)

## Format your text by surrounding it in `<tags>`

1. Turn your name into a top level heading by surrounding it in `<h1>` tags
Write the following into your IDE, save it, then refresh the browser.

```html
<h1>Shaun Cechner</h1>
```
{{%expand "Preview" %}}
<div class="reset preview">
  <h1>Shaun Cechner</h1>
</div>
{{% /expand %}}

2. Experiment with the tags

```html
<h1>Shaun</h1> Cechner
```
{{%expand "Preview" %}}
<div class="reset preview">
  <h1>Shaun</h1> Cechner
</div>
{{% /expand %}}

```html
<h1>
  Shaun Cechner
</h1>
```
{{%expand "Preview" %}}
<div class="reset preview">
  <h1>
    Shaun Cechner
  </h1>
</div>
{{% /expand %}}

3. Some other `<tags>` to try
- `<h2>`**Heading 2**`</h2>`  
- `<h6>`**Heading 6**`</h6>`  
- `<p>`For a paragraph of text`</p>`
- `<b>`**To make some text bold**`</b>`
- `<small>`<small>For small text</small>`</small>`
- `<em>`*For emphasized text*`</em>`

{{%expand "Preview" %}}
<div class="reset preview">
  <h2>Heading 2</h2>  
  <h6>Heading 6</h6>
  <p>For a paragraph of text</p>
  <b>To make some text bold</b><br>
  <small>For small text</small><br>
  <em>For emphasized text</em>
</div>
{{% /expand %}}

The following is an example of some HTML formatting.
```html
<h1>Shaun Cechner</h1>
<p>
    <em>Sometimes</em> people put <code>code</code>
    on separate lines to make things <b>easier to read</b>.
</p>
<p><small>only if they want to.</small></p>
```

{{%expand "Preview" %}}
<div class="reset preview">
  <h1>Shaun Cechner</h1>
  <p>
      <em>Sometimes</em> people put <code>code</code>
      on separate lines to make things <b>easier to read</b>.
  </p>
  <p><small>only if they want to.</small></p>
</div>
{{% /expand %}}

Some tags don't have an `<open>` and `</close>` tag, like if you want to put a horizontal rule in, it is done with 1 self-closing tag `<hr />`

```html
<h1>Shaun Cechner</h1>
<hr />
<p>
    <em>Sometimes</em> people put <code>code</code>
    on separate lines to make things <b>easier to read</b>.
</p>
<p><small>only if they want to.</small></p>
```

{{%expand "Preview" %}}
<div class="reset preview">
  <h1>Shaun Cechner</h1>
  <hr />
  <p>
      <em>Sometimes</em> people put <code>code</code>
      on separate lines to make things <b>easier to read</b>.
  </p>
  <p><small>only if they want to.</small></p>
</div>
{{% /expand %}}