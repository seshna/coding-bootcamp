---
title: "Project: My Projects"
draft: false
weight: 6
---

## My personal projects
Keep account of your projects

1. Create a new file, call it `projects.html`  
    ```bash
    Documents
    └── src                     
        └── coding-bootcamp
            └── index.html
            └── projects.html <- new file
    ```
2. Open the file and insert the template html code tags
  `doctype, html, head, body`
  {{%expand "Click for the answer" %}}
  ```html
  <!DOCTYPE html>
  <html>
    <head>
    </head>

    <body>
    </body>
  </html> 
  ```
  {{% /expand%}}
3. In the `<head>` section, create `<title>` tags enclosing your name and "projects", eg. "Shaun's Projects".  
  Can you see where the title is displayed?
  {{% expand "Click for the answer" %}}
  ```html
  <!DOCTYPE html>
  <html>
    <head>
      <title>Shaun's Projects</title>
    </head>

    <body>
    </body>
  </html> 
  ```    
  {{% /expand%}}
  ![Simple table](https://i.imgur.com/7Qhsfux.png?width=30pc&classes=shadow&featherlight=true)
4. Create a heading in the `<body>` section, call it your name and "projects", eg. "Shaun's Projects"
  {{% expand "Click for the answer" %}}
  ```html
  <!DOCTYPE html>
  <html>
    <head>
      <title>Shaun's Projects</title>
    </head>

    <body>
      <h1>Shaun's Projects</h1>
    </body>
  </html> 
  ```
  {{% /expand%}}

5. Create a table that looks similar to below, but with your own projects
  ![Simple table](https://i.imgur.com/V494RHf.png?width=30pc&classes=shadow&featherlight=true)
  {{% expand "Click for the answer" %}}
  ```html
  <!DOCTYPE html>
  <html>
    <head>
      <title>Shaun's Projects</title>
    </head>

    <body>
      <h1>Shaun's Projects</h1>
      <table border=1 cellpadding=10px>
        <thead>
          <tr>
            <th>Project</th>
            <th>Description</th>
          </tr>
        </thead>

        <tbody>
          <tr>
            <td>Garden beds</td>
            <td>Run reticulation to the garden beds</td>
          </tr>
        </tbody>
      </table>
    </body>
  </html>
  ```
  {{% /expand%}}
